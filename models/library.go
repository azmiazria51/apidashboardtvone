package models

type ResponseGeneral struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}

type Usernames struct {
	Username string `json:"username,omitempty"`
	Email    string `json:"email,omitempty"`
	Password string `json:"password,omitempty"`
}
