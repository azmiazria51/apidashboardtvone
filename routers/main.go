package main

import (
	"dashboardtvone/controllers"
	"net/http"
)

func main() {
	http.HandleFunc("/register", controllers.Register)
	http.HandleFunc("/login", controllers.Login)
	http.ListenAndServe(":1802", nil)
}
