package controllers

import (
	"crypto/sha256"
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"
	"regexp"

	"dashboardtvone/models"

	_ "github.com/go-sql-driver/mysql"
)

func connect() (*sql.DB, error) {
	db, err := sql.Open("mysql", "root:password@tcp(127.0.0.1:3306)/dashboardtvone")
	if err != nil {
		return nil, err
	}
	return db, nil
}

func Register(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", "Access-Control-Allow-Origin, Origin, X-Requested-With, Content-Type, Accept, Authorization")

	if r.Method != "POST" {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("500 - Something bad happened!"))
	} else {

		var Usernames = r.FormValue("username")
		var Firstname = r.FormValue("firstname")
		var Lastname = r.FormValue("lastname")

		var password = r.FormValue("password")
		var sha = sha256.New()
		sha.Write([]byte(password))
		var encrypted = sha.Sum(nil)
		var encryptedString = fmt.Sprintf("%x", encrypted)
		pswd := encryptedString

		var confirmpass = r.FormValue("confirmpass")
		var shas = sha256.New()
		shas.Write([]byte(confirmpass))
		var encrypteds = shas.Sum(nil)
		var encryptedStrings = fmt.Sprintf("%x", encrypteds)
		confirmpswd := encryptedStrings

		var Email = r.FormValue("email")
		var Nomorwa = r.FormValue("nomor_wa")

		var response models.ResponseGeneral

		db, err := connect()
		if err != nil {
			fmt.Println(err.Error())
			return
		}
		defer db.Close()

		var pesan = ""
		var statuskode = ""

		if Usernames == "" || Firstname == "" || Lastname == "" || password == "" || confirmpass == "" || Email == "" || Nomorwa == "" {

			statuskode = "400"
			pesan = "Mohon lengkapi data"

		} else {
			if password != confirmpass {

				pesan = "password dan konfirmasi password tidak sama"
				statuskode = "401"

			} else {

				_, err = db.Exec("INSERT INTO tbl_user (username,firstname,lastname,password,confirmpass,email,nomor_wa) values(?,?,?,?,?,?,?)", Usernames, Firstname, Lastname, pswd, confirmpswd, Email, Nomorwa)
				if err != nil {
					fmt.Println(err.Error())
					return
				}

				pesan = "Registrasi Berhasil"
				statuskode = "200"

			}
		}
		response.Status = statuskode
		response.Message = pesan
		json.NewEncoder(w).Encode(response)
	}
}

var emailRegex = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")

func isEmailValid(e string) bool {
	if len(e) < 3 && len(e) > 254 {
		return false
	}
	return emailRegex.MatchString(e)
}

func Login(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", "Access-Control-Allow-Origin, Origin, X-Requested-With, Content-Type, Accept, Authorization")

	if r.Method != "POST" {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("500 - Something bad happened!"))
	} else {
		db, err := connect()
		if err != nil {
			fmt.Println(err.Error())
			return
		}
		defer db.Close()

		Email := r.FormValue("email")

		Pswd := r.FormValue("password")
		var sha = sha256.New()
		sha.Write([]byte(Pswd))
		var encrypted = sha.Sum(nil)
		var encryptedString = fmt.Sprintf("%x", encrypted)
		pswds := encryptedString

		//get username
		dataUser, err := db.Query("Select username,email,password from tbl_user where password = ? and email = ?", pswds, Email)
		if err != nil {
			fmt.Println(err.Error())
			return
		}

		var data models.Usernames
		var response models.ResponseGeneral
		var pesan = ""
		var statuskode = ""

		//validasi format email
		if isEmailValid(Email) {
			if dataUser.Next() {
				var err = dataUser.Scan(&data.Username, &data.Email, &data.Password)
				if err != nil {
					fmt.Println(err.Error())
					return
				}
			}
			emails := data.Email
			pass := data.Password
			username := data.Username

			if Email != emails || pswds != pass {
				statuskode = "402"
				pesan = "email atau password salah"
			} else {
				_, err = db.Exec("INSERT INTO tbl_login (username,status) values(?,'1')", username)
				if err != nil {
					fmt.Println(err.Error())
					return
				}
				statuskode = "200"
				pesan = "berhasil login"
			}
		} else {
			statuskode = "403"
			pesan = "format email salah"
		}

		response.Status = statuskode
		response.Message = pesan
		json.NewEncoder(w).Encode(response)
	}
}
